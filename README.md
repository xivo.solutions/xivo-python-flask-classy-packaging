# The packaging information for python-flask-classy in XiVO

This repository contains the packaging information for
[python-flask-classy](http://pythonhosted.org/Flask-Classy/).

To get a new version of python-consul in the XiVO repository, set the desired
version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
